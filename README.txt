This module is the result of the conversation in
http://www.ubercart.org/forum/support/9952/need_remove_role_purchase.

**********
*Use case*
**********

We have a product Silver associated with a Silver role and a product Gold
associated with a Gold role. We also have other roles in our site independent of
memberships.
*A user buys a Silver Membership.
*A few days later, before the actual expiration of the Silver Membership, the
user decides to buy the Gold Membership.
*By default, the user will have both Silver and Gold roles.
*UC Clear Roles is responsible for fixing exactly this by removing the Silver
role. Ubercart does the rest.

Notice:
In the example above, the user loses all days he would normally be allowed to
remain as Silver and immediately changes to Gold as a new membership and not as
a renewal.. In case a Gold user buys another Gold membership product before the
actual expiration of the first package, this module does nothing and expiration
time gets appended(and not resetted).

This module also respects other roles that may exist in your system but are
independent of memberships(ex. admin, article-writer, editor). For choosing
which roles are memberships and which are not, an administration page exists for
the administrator to choose.

UC Clear Roles works with Conditional Actions and not with Rules. Therefore, it
is only applicable to Ubercart 2.x and Drupal 6.x. There is no plan right now to
make a port to D7.
